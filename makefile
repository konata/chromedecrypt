LIBS=-lcrypto -lsqlite3 `pkg-config --cflags --libs libsecret-1`
chromedecrypt: main.c aes.c fastpbkdf2.c
	$(CC) $^ $(LIBS) -o $@
debug: main.c aes.c fastpbkdf2.c
	$(CC) -g -ggdb $^ $(LIBS) -o $@
