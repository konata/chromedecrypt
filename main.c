#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include<sqlite3.h>
#include<string.h>
#include"aes.h"
#include"fastpbkdf2.h"

#include<libsecret/secret.h>

static const SecretSchema * myschema_get(void) G_GNUC_CONST;
static char *getchromekey(void);
static int sqlite_callback(void *data, int argc, char **argv, char **colname);
static uint8_t *decrypt(uint8_t *pass, size_t nbytes);
static void copydb(void);

#define CHROMESCHEMA chromeschema_get()
// create chrome schema for the secretservice
const SecretSchema *chromeschema_get(void) {
	// i got this schema from the
	// chromium source code
	static const SecretSchema schema = {
		"chrome_libsecret_os_crypt_password_v2", SECRET_SCHEMA_DONT_MATCH_NAME,
		{
			{"application", SECRET_SCHEMA_ATTRIBUTE_STRING},
			{NULL, SECRET_SCHEMA_ATTRIBUTE_STRING}
		}
	};
	return &schema;
}

// get oscrypt stored
// in the secret service
char *getchromekey(void) {
	GError *error = NULL;
	gchar *password = secret_password_lookup_sync(CHROMESCHEMA, NULL, &error,
			"application", "chrome",
			NULL);
	if(error != NULL) {
		puts("Error getting pass");
		g_error_free(error);
	}

	return password;
}

int sqlite_callback(void *data, int argc, char **argv, char **colname) {
	for(int i = 0; i < argc; ++i) {
		// show all columns and if its the
		// password, show it decrypted
		if(strcmp("password_value", colname[i]) == 0) {
			size_t len = strlen(argv[i]);
			uint8_t pass[len];
			for(int k = 0; k < len; ++k)
				pass[k] = argv[i][k];
			printf("%s: %s\n", colname[i], decrypt(pass, len));
		} else
			printf("%s: %s\n", colname[i], argv[i] ? argv[i] : "NULL");
	}
		puts("---------------------");
	return 0;
}

// decrypt login pass (v11 only for now...) 
uint8_t *decrypt(uint8_t *pass, size_t nbytes) {
	// this offset is to malloc() without the v10/v11
	// and so there is space for the last '\0'
	int offset = nbytes-2;
	uint8_t *encrypted_pass = (uint8_t*)malloc(sizeof(uint8_t)*offset);
	memset(encrypted_pass, '\0', offset);
	// remove the v11 suffix
	for(int i = 3; i < nbytes; ++i)
		encrypted_pass[i-3] = pass[i];

	// secret service chrome password
	//uint8_t pw[] = {"peanuts"}; // this is for v10 passwords
	uint8_t *pw = getchromekey();
	uint8_t sl[] = {"saltysalt"};

	// the password+salt based key
	uint8_t ot[16] = {0};
	fastpbkdf2_hmac_sha1(pw, 24, sl, 9, 1, ot, 16);

	// finally decrypt
	struct AES_ctx ctx;
	uint8_t iv[16] = {
		' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
		' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
	};
	AES_init_ctx_iv(&ctx, ot, iv);
	AES_CBC_decrypt_buffer(&ctx, encrypted_pass, nbytes-3);

	return encrypted_pass;
}

void copydb(void) {
	// create the path to the database
	char *user = getenv("HOME");
	char *gpath = "/.config/google-chrome/Default/Login Data"; 
	char fullpath[300] = {'\0'};
	strcat(fullpath, user);
	strcat(fullpath, gpath);

	FILE *f = fopen(fullpath, "r");
	FILE *w = fopen("/tmp/sqlite3db", "w");

	// copy file to new location
	int c;
	while((c = getc(f)) != EOF)
		fputc(c, w);

	fclose(f);
	fclose(w);
}

int main(void) {
	copydb();

	// sqlite variables
	sqlite3 *db = NULL;
	char *query = "select origin_url, username_value, password_value from logins";
	char *err = 0;
	int rc;

	// open sqlite database
	rc = sqlite3_open("/tmp/sqlite3db", &db);
	if(rc) {
		printf("cannot open db: %s\n", sqlite3_errmsg(db));
		return -1;
	}

	// execute query
	rc = sqlite3_exec(db, query, sqlite_callback, NULL, &err);
	if(rc != SQLITE_OK) {
		printf("sqlite3_exec: %s\n", err);
		sqlite3_free(err);
		return -1;
	}

	sqlite3_close(db);
	return 0;
}

